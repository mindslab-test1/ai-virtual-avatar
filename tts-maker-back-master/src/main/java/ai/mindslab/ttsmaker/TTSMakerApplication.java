package ai.mindslab.ttsmaker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class TTSMakerApplication {
	public static void main(String[] args) {
		SpringApplication.run(TTSMakerApplication.class, args);
	}
}
