package ai.mindslab.ttsmaker.configurers.resolvers;

import ai.mindslab.ttsmaker.api.user.vo.response.UserVo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@AllArgsConstructor
public class AvaUser {
	private Long userNo;
	private String userId;
	private String userNm;
	private String companyNm;
	private Integer planNo;
	
	
	private String accessToken;
	private String accessTokenExpireDate;
	
	private String refreshToken;
	private String refreshTokenExpireDate;
	
	public static String userPrint(AvaUser avaUser) {
		if(avaUser != null ) {
			return String.format("user: %s(%s/%s)", avaUser.getUserNo(), avaUser.getUserId(), avaUser.getUserNm());
		}
		return "AvaUser=UNKNOWN";
	}
	
	
	public static AvaUser of(UserVo entity) {
        return AvaUser.builder()
        		.userNo(entity.getUserNo())
        		.userId(entity.getUserId())
        		.userNm(entity.getUserNm())
        		.companyNm(entity.getCompanyNm())
        		.planNo(entity.getPlanNo())
        		.build();
    }
	public static AvaUser empty() {
        return AvaUser.builder()
        		.userNo(0l)
        		.userId("GUEST")
        		.userNm("GUEST")
        		.build();
    }
}
