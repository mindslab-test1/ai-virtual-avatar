package ai.mindslab.ttsmaker.configurers.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import ai.mindslab.ttsmaker.api.user.vo.response.UserVo;
import ai.mindslab.ttsmaker.common.AVAConst;
import ai.mindslab.ttsmaker.configurers.auth.AVAAuthConst;
import ai.mindslab.ttsmaker.configurers.auth.UserPrincipal;
import ai.mindslab.ttsmaker.configurers.auth.UserPrincipalDetailsService;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import io.jsonwebtoken.Claims;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {
	
	private UserPrincipalDetailsService userPrincipalDetailsService;
	
	public JwtAuthorizationFilter(AuthenticationManager authenticationManager, UserPrincipalDetailsService userPrincipalDetailsService) {
        super(authenticationManager);
        this.userPrincipalDetailsService = userPrincipalDetailsService;
	}
	
	@Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(AVAAuthConst.AUTH_HEADER_STRING);
        if(header == null || !header.startsWith(AVAAuthConst.AUTH_TOKEN_PREFIX)){
            chain.doFilter(request,response);
            return;
        }
        try {
        	Authentication authentication = getUsernamePasswordAuthentication(request);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }catch(AuthenticationException e) {
            System.out.println("Authentication failed: " + e.getMessage());
        }

        // Continue filter execution
        chain.doFilter(request, response);
	}
	
	private Authentication getUsernamePasswordAuthentication(HttpServletRequest request) {
        String token = request.getHeader(AVAAuthConst.AUTH_HEADER_STRING);
        if(token != null) {
        	String tokenBody = token.replace(AVAAuthConst.AUTH_TOKEN_PREFIX,"");
        	if(userPrincipalDetailsService.validateJwtToken(tokenBody, request)) {
        		// 리디스 
        		UserVo userVo = userPrincipalDetailsService.getUserVoFromRedis(tokenBody);
        		UserPrincipal principal = null;
        		if(userVo != null) {
        			principal = UserPrincipal.create(userVo);
        		}else {
        			Claims claims = userPrincipalDetailsService.getClaims(tokenBody);
        			String email = claims.getSubject();
            		principal = (UserPrincipal) userPrincipalDetailsService.loadUserByUsername(email);
        		}
        		
        		request.setAttribute(AVAConst.AVA_INFO, AvaUser.of(principal.getUser()));
        		return new UsernamePasswordAuthenticationToken(principal.getUsername(), null, principal.getAuthorities());
        	}
        }
        return null;
	}
}
