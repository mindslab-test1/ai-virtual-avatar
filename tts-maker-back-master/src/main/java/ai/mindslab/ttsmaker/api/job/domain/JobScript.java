package ai.mindslab.ttsmaker.api.job.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Builder
@Table(name = "JOB_SCRIPT")
public class JobScript implements Serializable {
	
	private static final long serialVersionUID = 9182436812539983497L;

	//관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private Long id;
    
    //JOB ID
    @Column(name = "JOB_ID", columnDefinition = "varchar(36)", nullable = false)
    private String jobId;
    
    //SCRIPT_ID
    @Column(name = "SCRIPT_ID", columnDefinition = "varchar(36)", nullable = false)
    private String scriptId;
    
    //화자 ID
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CHARACTER_ID", columnDefinition = "varchar(36)", referencedColumnName = "CHARACTER_ID", nullable = false, foreignKey = @ForeignKey(name = "CHARACTER_FK2"))
    private VFCharacter vfCharacter;
    
    // TTS 스크립트 Text
    @Column(name = "SCRIPT_TEXT", columnDefinition = "TEXT", nullable = false)
    private String scriptText;
    
    // TTS Resource URL정보
    @Column(name = "TTS_URL", columnDefinition = "varchar(1000)")
    private String ttsUrl;
    
    // TTS Resource 만료일
    @Column(name = "TTS_EXPIRED_DATE", columnDefinition = "varchar(10)")
    private String ttsExpiredDate;
    
    //정렬순서
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_SEQ", columnDefinition = "int(11)")
    private Long orderSeq;
    
    //삭제여부
    @Column(name = "DEL_YN", columnDefinition = "char(1) default 'N'")
    private String isDeleted;
    
    //TTS 상태
    @Column(name = "TTS_STATE", columnDefinition = "varchar(10) default 'INIT'")
    private String ttsState;
    
    //ROW 상태
    @Column(name = "ROW_STATE", columnDefinition = "varchar(10) default 'S'")
    private String rowState;
    
    //등록일시
    @Column(name = "INS_DT")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;

    //수정일시
    @Column(name = "UPD_DT")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
}
