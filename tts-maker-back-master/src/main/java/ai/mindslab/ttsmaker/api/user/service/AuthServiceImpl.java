package ai.mindslab.ttsmaker.api.user.service;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.user.domain.UserRefreshToken;
import ai.mindslab.ttsmaker.api.user.repository.UserRefreshTokenRepository;
import ai.mindslab.ttsmaker.util.DateUtils;

@Service
public class AuthServiceImpl implements AuthService {
	
	@Autowired
	private UserRefreshTokenRepository userRefreshTokenRepository;
	
	@Override
	public boolean saveRefreshToken(Long userNo, String refreshToken, Date refreshTokenExpireDate) {
		Optional<UserRefreshToken> userRefreshTokenOpt = userRefreshTokenRepository.findByRefreshToken(refreshToken);
		
		boolean save = false;
		try {
			UserRefreshToken userRefreshToken = null;
			if(userRefreshTokenOpt.isPresent()) {
				userRefreshToken = userRefreshTokenOpt.get();
				userRefreshToken.setRefreshTokenExpireDate(DateUtils.convertToLocalDateTimeViaInstant(refreshTokenExpireDate));
			}else {
				userRefreshToken = new UserRefreshToken();
				userRefreshToken.setUserNo(userNo);
				userRefreshToken.setRefreshToken(refreshToken);
				userRefreshToken.setRefreshTokenExpireDate(DateUtils.convertToLocalDateTimeViaInstant(refreshTokenExpireDate));
			}
			userRefreshTokenRepository.save(userRefreshToken);
			save = true;
		}catch (Exception e) {
			throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRSU002", "userRefreshToken 생성중 오류가 발생 했습니다.");
		}
		
		return save;
	}

	@Override
	public boolean setRefreshTokenBlocklist(Long userNo, String refreshToken) {
		userRefreshTokenRepository.setRefreshTokenBlocklist(refreshToken);
		return true;
	}
}
