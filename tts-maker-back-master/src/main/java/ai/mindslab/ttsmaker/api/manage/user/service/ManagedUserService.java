package ai.mindslab.ttsmaker.api.manage.user.service;

import org.springframework.data.domain.PageRequest;

import ai.mindslab.ttsmaker.api.manage.user.vo.reponse.UserListReponseDto;
import ai.mindslab.ttsmaker.api.manage.user.vo.request.UserSearchRequestDto;
import ai.mindslab.ttsmaker.common.page.AvaPage;

public interface ManagedUserService {
	public AvaPage<UserListReponseDto> getList(PageRequest pageable, UserSearchRequestDto searchDto);
}
