package ai.mindslab.ttsmaker.api.plan.domain;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import ai.mindslab.ttsmaker.configurers.converter.PlanBenefitConverter;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "PLAN")
public class Plan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO", columnDefinition = "int(11)")
    private int planNo;

    @Column(name = "PLAN_NM", columnDefinition = "varchar(100)")
    private String planNm;

    @Column(name = "PRICE", columnDefinition = "int(11)")
    private Long planPrice;

    @Column(name = "MAX_LIMIT", columnDefinition = "int(11)")
    private Long maxLimit;
    
    
    @Column(name = "PLAN_BENEFIT", columnDefinition="json default '{}'")
    @Convert(converter = PlanBenefitConverter.class)
    private List<String> planBenefit;
    
    @Column(name = "EXPOSE_YN", columnDefinition = "char(1) default 'N'")
    private String exposeYn;

    @Column(name = "ACTIVE", columnDefinition = "int(1)")
    private int active;

    //등록일시
    @Column(name = "CREATE_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createDate;

    //수정일시
    @Column(name = "UPDATE_DATE")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updateDate;
}
