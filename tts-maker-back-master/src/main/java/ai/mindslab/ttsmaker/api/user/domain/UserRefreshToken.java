package ai.mindslab.ttsmaker.api.user.domain;


import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@DynamicInsert 
@DynamicUpdate 
@Getter
@Setter
@Entity
@Table(name = "USER_REFRESH_TOKEN")
public class UserRefreshToken {

    //관리자번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO", columnDefinition = "int(11)")
    private Long no;

    //user no
    @Column(name = "USER_NO", columnDefinition = "int(11)")
    private Long userNo;

    // REFRESH_TOKEN
    @Column(name = "REFRESH_TOKEN", columnDefinition = "varchar(200)")
    private String refreshToken;
    
    @Column(name = "EXPIRE_DATE")
    private LocalDateTime refreshTokenExpireDate;
    
    @Column(name = "ACTIVE", columnDefinition = "boolean default true")
    private Boolean active;
    
    //등록일시
    @Column(name = "CREATE_DATE")
    @CreationTimestamp
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createDate;

    //수정일시
    @Column(name = "UPDATE_DATE")
    @UpdateTimestamp
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedDate;
}
