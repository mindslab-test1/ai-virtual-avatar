package ai.mindslab.ttsmaker.api.job.repository;

import ai.mindslab.ttsmaker.api.job.domain.JobScript;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface JobScriptRepository extends JpaRepository<JobScript, Long> {
	
	@Query("select p from JobScript p where p.isDeleted = 'N' and p.jobId = ?1")
	List<JobScript> getActiveScriptListFindByJobId(String jobId, Sort sort);
	
	@Query("select p from JobScript p where p.isDeleted = 'N' and p.jobId = ?1 and p.scriptId = ?2")
	JobScript getJobScriptFindByJobIdAndScriptId(String jobId, String scriptId);

	int deleteByIsDeleted(String isDeleted);
}
