package ai.mindslab.ttsmaker.api.user.vo.response;

import java.io.Serializable;

import ai.mindslab.ttsmaker.api.user.domain.UserM;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserVo implements Serializable {
	private static final long serialVersionUID = 5374884932662896408L;
	private Long userNo;
	private String userId;
	private String userPw;
	private String userNm;
	private String companyNm;
	private Integer planNo;
	private Boolean active;
	
	public static UserVo of(UserM entity) {
        return UserVo.builder()
        		.userNo(entity.getUserNo())
        		.userId(entity.getUserId())
        		.userPw(entity.getUserPw())
        		.userNm(entity.getUserNm())
        		.companyNm(entity.getCompanyNm())
        		//.planNo(entity.getProductId())
        		.active(entity.getActive())
        		.build();
    }
}
