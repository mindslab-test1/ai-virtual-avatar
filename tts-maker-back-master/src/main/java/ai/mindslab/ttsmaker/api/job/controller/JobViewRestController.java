package ai.mindslab.ttsmaker.api.job.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.job.service.JobService;
import ai.mindslab.ttsmaker.api.job.vo.response.JobListVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobScriptVo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/job/view")
public class JobViewRestController {
	
	@Autowired
	private JobService jobService;
	
	@ResponseBody
	@GetMapping(path = "/list")
	public List<JobListVo> list() throws IOException {
		return jobService.getJobList();
	}
	
	@ResponseBody
	@GetMapping(path = "/{jobId}")
	public List<JobScriptVo> getJobScriptList(@PathVariable String jobId) {
		log.info("[Get List of Job Scripts] : {}", jobId);
		return jobService.getJobScriptList(jobId);
	}
	
	@ResponseBody
	@GetMapping(path = "/{jobId}/scripts")
	public List<JobScriptVo> getJobScriptListFindByJobId(@PathVariable String jobId) throws IOException {
		log.info("[Listing Job Script] : {}", jobId);

		return jobService.getJobScriptList(jobId);
	}
	
	@ResponseBody
	@GetMapping(value = "/{jobId}/{scriptId}")
	public JobScriptVo getJobScript(@PathVariable String jobId, @PathVariable String scriptId) {
		log.info("[Get Job Script] : {}, ", jobId, scriptId);
		return jobService.getJobScript(jobId, scriptId);
	}
}
