package ai.mindslab.ttsmaker.api.manage.user.vo.request;

import ai.mindslab.ttsmaker.common.page.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public final class UserSearchRequestDto extends PageRequest {
	@ApiModelProperty(name = "userNm",  example = "", value = "사용자 이름" )
	private String userNm;
	
	@ApiModelProperty(name = "userEmail",  example = "", value = "사용자 이메일" )
	private String userEmail;
	
	@ApiModelProperty(name = "userCellPhone",  example = "", value = "사용자 연락처" )
	private String userCellPhone;
	
	@ApiModelProperty(name = "companyNm",  example = "", value = "회사 이름" )
	private String companyNm;
	
	@ApiModelProperty(name = "planCd", example = "", value="PLAN 구분\n 1:LITE, 2:STANDARD, 3:BUSINESS, 4:Free")
	private String planCd;
}
