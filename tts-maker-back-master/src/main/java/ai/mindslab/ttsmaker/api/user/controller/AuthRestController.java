package ai.mindslab.ttsmaker.api.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.user.service.AuthService;
import ai.mindslab.ttsmaker.api.user.vo.request.UserAuthRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserAuthResponse;

@RestController
@RequestMapping("/user")
public class AuthRestController {
	
	@Autowired
	private AuthService authService;
		
	@ResponseBody
    @PostMapping(value = "/sign_out")
	public UserAuthResponse logout(@RequestBody UserAuthRequestVo authRequestVo) {
		authService.setRefreshTokenBlocklist(authRequestVo.getUserNo(), authRequestVo.getRefreshToken());
		return new UserAuthResponse(authRequestVo.getUserNo(), "로그아웃 처리 되었습니다.");
	}
	
	@ResponseBody
    @PostMapping(value = "/auth/refreshToken")
	public String generatorRefreshToken() {
		// TODO 
		return null;
	}
}
