package ai.mindslab.ttsmaker.api.job.vo.response;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.job.domain.JobScript;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobScriptVo {
	private long id;
	private String jobId;
	private String scriptId;
	private String characterId;
	private String characterNm;
	private String scriptText;
	private String ttsUrl;
	private String ttsExpiredDate;
	private long orderSeq;
	private String ttsState;
	private String rowState;
	
	public static JobScriptVo of(JobScript entity) {
		VFCharacter character = entity.getVfCharacter();
        return JobScriptVo.builder()
        		.id(entity.getId())
        		.jobId(entity.getJobId())
        		.scriptId(entity.getScriptId())
        		.characterId(character.getCharacterId())
        		.characterNm(character.getCharacterNm())
        		.scriptText(entity.getScriptText())
        		.ttsUrl(entity.getTtsUrl())
        		.ttsExpiredDate(entity.getTtsExpiredDate())
        		.orderSeq(entity.getOrderSeq())
        		.ttsState(entity.getTtsState())
        		.rowState(entity.getRowState())
        		.build();
    }
}
