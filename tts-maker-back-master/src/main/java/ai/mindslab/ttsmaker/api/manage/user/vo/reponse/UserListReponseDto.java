package ai.mindslab.ttsmaker.api.manage.user.vo.reponse;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.mindslab.ttsmaker.api.user.domain.UserM;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserListReponseDto {
	private long userNo;
	private String userNm;
	private String userEmail;
	private String userCellPhone;
	private String companyNm;
		
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMddHHmmss")
	private LocalDateTime createAt;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMddHHmmss")
	private LocalDateTime updateAt;
	
	public static UserListReponseDto of(UserM entity) {
		//Product product = entity.getProduct();
		
		return UserListReponseDto.builder()
				.userNo(entity.getUserNo())
				.userNm(entity.getUserNm())
				.userEmail(entity.getUserId())
				.userCellPhone(entity.getCellphone())
				.companyNm(entity.getCompanyNm())
				.createAt(entity.getCreateDate())
				.updateAt(entity.getUpdatedDate())
				.build();
	}
	
}
