package ai.mindslab.ttsmaker.api.user.service;

import java.sql.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.domain.UserRefreshToken;
import ai.mindslab.ttsmaker.api.user.repository.UserMRepository;
import ai.mindslab.ttsmaker.api.user.repository.UserRefreshTokenRepository;
import ai.mindslab.ttsmaker.api.user.vo.request.UserSignUpRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserSignUpResponseVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserVo;
import ai.mindslab.ttsmaker.util.DateUtils;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserMRepository userMRepository;
	
	@Override
	public UserVo getUserVoFindByUserId(String userId) {
		Optional<UserM> userOpt = userMRepository.findByUserId(userId);
		if(userOpt.isPresent()) {
			return UserVo.of(userOpt.get());
		}
		
		return null;
	}

	@Override
	public UserSignUpResponseVo userSignUp(UserSignUpRequestVo signUpVo) {
		Optional<UserM> userOpt = userMRepository.findByUserId(signUpVo.getUserId());
		if(userOpt.isPresent()) {
			// TODO : 중복 체크 처리
		}
		
		UserM userM = new UserM();
		userM.setUserId(signUpVo.getUserId());
		userM.setUserPw(passwordEncoder.encode(signUpVo.getUserPw()));
		userM.setUserNm(signUpVo.getUserNm());
		userM.setCompanyNm(signUpVo.getCompanyNm());
		userM.setCellphone(signUpVo.getCellphone());
		
		userMRepository.save(userM);
		
		// TODO : userSignUp 처리 후 데이터
		return null;
	}
}
