package ai.mindslab.ttsmaker.api.provider.maumai.tts.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "tts")
@Getter
@Setter
public class TTSApiProperties {
	 private String API_ID;
	 private String API_KEY;
}
