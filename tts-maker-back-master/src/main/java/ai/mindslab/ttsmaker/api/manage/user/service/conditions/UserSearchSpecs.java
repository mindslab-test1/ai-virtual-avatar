package ai.mindslab.ttsmaker.api.manage.user.service.conditions;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;

import ai.mindslab.ttsmaker.api.manage.user.vo.request.UserSearchRequestDto;
import ai.mindslab.ttsmaker.api.user.domain.UserM;

public class UserSearchSpecs {
	
	public static Specification<UserM> chainingSpecs(UserSearchRequestDto searchDto) {
		
		List<Specification<UserM>> specs = Lists.newArrayList();
		// 사용자 이름 조회
		if(StringUtils.hasText(searchDto.getUserNm())) {
			specs.add(userNmLike(searchDto.getUserNm()));
		}
		
		// 사용자 E-mail 조회
		if(StringUtils.hasText(searchDto.getUserEmail())) {
			specs.add(userEmailLike(searchDto.getUserEmail()));
		}
		
		// 사용자 연락처 조회
		if(StringUtils.hasText(searchDto.getUserCellPhone())) {
			specs.add(userEmailLike(searchDto.getUserCellPhone()));
		}
		
		// 회사 이름 조회
		if(StringUtils.hasText(searchDto.getCompanyNm())) {
			specs.add(companyNmLike(searchDto.getCompanyNm()));
		}
		
//		// 구독 상품 조회
//		if(StringUtils.hasText(searchDto.getPlanCd())) {
//			specs.add(planCdEqual(ai.mindslab.ttsmaker.util.StringUtils.toInt(searchDto.getPlanCd())));
//		}
		
		Specification<UserM> specification = Specification.where(null);
		if(!CollectionUtils.isEmpty(specs)) {
			specification = specs.stream().reduce(specification, (specs1, specs2) -> specs1.and(specs2));
		}
		return specification;
	}
	
	/**
	 * <pre>
	 * 사용자 이름 조회(Specification)
	 * <pre> 
	 * @param userNm
	 * @return
	 */
	public static Specification<UserM> userNmLike(final String userNm) {
		 return (Specification<UserM>) ((root, query, builder) -> {
			 return builder.like(root.get("userNm"), "%" + userNm + "%");
		 });
    }
	
	/**
	 * <pre>
	 * 사용자 이메일 조회(Specification)
	 * <pre> 
	 * @param userEmail
	 * @return
	 */
	public static Specification<UserM> userEmailLike(final String userEmail) {
		 return (Specification<UserM>) ((root, query, builder) -> {
			 return builder.like(root.get("userId"), "%" + userEmail + "%");
		 });
    }
	
	/**
	 * <pre>
	 * 사용자 휴대폰 조회(Specification)
	 * <pre> 
	 * @param cellPhone
	 * @return
	 */
	public static Specification<UserM> userCellPhoneEqual(final String cellPhone) {
		 return (Specification<UserM>) ((root, query, builder) -> {
			 return builder.equal(root.get("cellphone"), cellPhone);
		 });
    }
	
	/**
	 * <pre>
	 * 회사 이름 조회(Specification)
	 * <pre> 
	 * @param userNm
	 * @return
	 */
	public static Specification<UserM> companyNmLike(final String companyNm) {
		 return (Specification<UserM>) ((root, query, builder) -> {
			 return builder.like(root.get("compNm"), "%" + companyNm + "%");
		 });
    }
}
