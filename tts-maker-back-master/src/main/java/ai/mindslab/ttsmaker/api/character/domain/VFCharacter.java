package ai.mindslab.ttsmaker.api.character.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "VF_CHARACTER", uniqueConstraints = {@UniqueConstraint(name = "UK_CHARACTER_ID", columnNames = { "CHARACTER_ID" }) })
public class VFCharacter implements Serializable {
	private static final long serialVersionUID = 2769514496241616924L;

	//관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private long id;
    
    //화자 ID
    @Column(name = "CHARACTER_ID", columnDefinition = "varchar(36)")
    private String characterId;
    
    //보이스폰트 이름
    @Column(name = "CHARACTER_NM", columnDefinition = "varchar(100)")
    private String characterNm;
 
    //보이스폰트 특징
    @Column(name = "CHARACTER_FEATURE", columnDefinition = "varchar(100)")
    private String characterFeature;
        
    //보이스폰트 언어스킬
    @Column(name = "CHARACTER_LANG", columnDefinition = "varchar(100)")
    private String characterLang;
    
    //보이스폰트 아바타 클래스 명
    @Column(name = "AVATAR_CLASS_NM", columnDefinition = "varchar(100)")
    private String avatarClassNm;
    
    //TTS 보이스 이름
    @Column(name = "VOICE_FONT_NM", columnDefinition = "varchar(100)")
    private String voiceFontNm;
    
    //TTS 보이스 Sample URL
    @Column(name = "VOICE_SAMPLE_URL", columnDefinition = "varchar(500)")
    private String voiceSampleUrl;
    
    //삭제여부
    @Column(name = "DEL_YN", columnDefinition = "char(1) default 'N'")
    private String isDeleted;
    
    //등록일시
    @Column(name = "INS_DT")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;

    //수정일시
    @Column(name = "UPD_DT")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
}
