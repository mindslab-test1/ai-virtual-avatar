package ai.mindslab.ttsmaker.api.provider.maumai.tts.service;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import ai.mindslab.ttsmaker.api.provider.maumai.tts.exception.MaumAIApiException;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.MaumAIRequestParam;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.MaumAIResponseFile;
import reactor.core.publisher.Mono;


@Service
public class TTSApiService {
    @Value("${api.baseUrl}")
    private String API_BASE_URL;
    
    @Autowired
    private TTSApiProperties ttsApiProperties;
   
    private static Logger logger = LoggerFactory.getLogger(TTSApiService.class.getName());
   
    public Mono<MaumAIResponseFile> requestTTSMakeFile(String voiceName, String text, String downloadId) {
        Float speed = 1.0f;
        UUID fileName = UUID.randomUUID();
        
        WebClient webClient = WebClient.builder()
        		.baseUrl(API_BASE_URL)
        		.defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
        		.defaultHeader(HttpHeaders.CACHE_CONTROL, "no-cache")
        		.build();

        MaumAIRequestParam param = new MaumAIRequestParam(ttsApiProperties.getAPI_ID(), ttsApiProperties.getAPI_KEY(), voiceName, text, speed, fileName.toString(), downloadId);
        logger.info("Request [{}]: {}", API_BASE_URL, param);

        return webClient.post().uri("/makeFile")
                .body(Mono.just(param), MaumAIRequestParam.class).retrieve()
                .onStatus(HttpStatus::isError, response -> {
                    MaumAIApiException exception = null;
                    HttpStatus statusCode = response.statusCode();
                    if (statusCode != null) {
                        switch (statusCode) {
                            case FORBIDDEN:
                                logger.error("{}: ERRA0001 - MaumAI API provider 인증 오류 ", response.statusCode());
                                exception = new MaumAIApiException(response.statusCode(), "ERRA0001", "MaumAI API provider 인증 오류");
                                break;
                            case BAD_GATEWAY:
                                logger.error("{}: ERRA0002 - MaumAI API provider와의 통신 오류", response.statusCode());
                                exception = new MaumAIApiException(response.statusCode(), "ERRA0002", "MaumAI API provider와의 통신 오류");
                                break;
                            case BAD_REQUEST:
                                logger.error("{}: ERRA0003 - The request parameter value is invalid", response.statusCode());
                                exception = new MaumAIApiException(response.statusCode(), "ERRA0003", "The request parameter value is invalid.(MaumAI API)");
                                break;
                            default:
                                if (statusCode.is4xxClientError() || statusCode.is5xxServerError()) {
                                    logger.error("{}: ERRA0000 - MaumAIAPI provider 통신 오류", response.statusCode());
                                    exception = new MaumAIApiException(response.statusCode(), "ERRA0000", "MaumAIAPI provider 통신 오류");
                                }
                                break;
                        }
                    } else {
                        exception = new MaumAIApiException(response.statusCode(), "ERRA0000", "MaumAIAPI provider 통신 오류");
                    }
                    return Mono.error(exception);
                })
                .bodyToMono(MaumAIResponseFile.class);
    }
}
