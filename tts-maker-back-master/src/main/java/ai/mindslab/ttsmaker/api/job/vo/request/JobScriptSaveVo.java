package ai.mindslab.ttsmaker.api.job.vo.request;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.job.domain.JobScript;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobScriptSaveVo {
	private String jobId;					// JOB ID
	private String scriptId;				// 스크립트 고유 번호
	private String characterId;				// 보이스 폰트 ID
	private String voiceName;
	private String scriptText;				// 스크립트 텍스
	private String ttsUrl;					// TTS Resource URL
	private String ttsExpiredDate;			// TTS Resource 만료
	private String ttsState;				// TTS 변환 상태
	private String rowState;				// Row 상태
	
	@JsonIgnore
	public static JobScriptSaveVo of(JobScript entity) {
		VFCharacter character = Optional.ofNullable(entity.getVfCharacter()).orElse(new VFCharacter());
		return JobScriptSaveVo.builder()
        		.jobId(entity.getJobId())
        		.scriptId(entity.getScriptId())
        		.characterId(character.getCharacterId())
        		.scriptText(entity.getScriptText())
        		.ttsUrl(entity.getTtsUrl())
        		.ttsExpiredDate(entity.getTtsExpiredDate())
        		.ttsState(entity.getTtsState())
        		.rowState(entity.getRowState())
        		.build();
    }
}
