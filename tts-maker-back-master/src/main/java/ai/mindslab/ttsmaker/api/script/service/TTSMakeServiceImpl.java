package ai.mindslab.ttsmaker.api.script.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.mindslab.ttsmaker.api.character.service.VFCharacterService;
import ai.mindslab.ttsmaker.api.character.vo.response.VFCharacterResponseVo;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.service.TTSApiService;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.MaumAIResponseFile;
import ai.mindslab.ttsmaker.api.script.domain.dto.response.TTSMakeResultVo;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class TTSMakeServiceImpl implements TTSMakeService {

	@Autowired
    private TTSApiService maumAiApiService;
	
	@Autowired
	private VFCharacterService vfCharacterService;

    public Mono<TTSMakeResultVo> requestTTSMakeFile(String characterId, String text) {
    	
    	// TODO : 이후 캐싱 처리 필요
    	List<VFCharacterResponseVo> voiceFontList = vfCharacterService.getAllList();
    	Map<String, String> voiceFontMap = voiceFontList.stream().collect(Collectors.toMap(VFCharacterResponseVo::getCharacterId, VFCharacterResponseVo::getVoiceFontNm));
    	
    	String voiceName = voiceFontMap.get(characterId);
    	log.info(voiceName);
        Mono<MaumAIResponseFile> result = maumAiApiService.requestTTSMakeFile(voiceName, text, null);

        Mono<TTSMakeResultVo> convertResult = result.map(data -> {
                    TTSMakeResultVo ttsMakeResult = TTSMakeResultVo.of(voiceName, text, data);
                    log.info("TTS Make File: {}", ttsMakeResult);
                    return ttsMakeResult;
                }
        );

        return convertResult;
    }
}
