package ai.mindslab.ttsmaker.api.script.service;

import ai.mindslab.ttsmaker.api.script.domain.dto.response.TTSMakeResultVo;
import reactor.core.publisher.Mono;

public interface TTSMakeService {
	public Mono<TTSMakeResultVo> requestTTSMakeFile(String characterId, String text);
}
