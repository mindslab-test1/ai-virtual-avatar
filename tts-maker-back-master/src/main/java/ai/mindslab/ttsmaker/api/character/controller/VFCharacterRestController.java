package ai.mindslab.ttsmaker.api.character.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.character.service.VFCharacterService;
import ai.mindslab.ttsmaker.api.character.vo.response.VFCharacterResponseVo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/characters")
public class VFCharacterRestController {

	@Autowired
	private VFCharacterService vfCharacterService;

	@ResponseBody
	@GetMapping
	public List<VFCharacterResponseVo> list() throws IOException {
		log.info("[Get Speaker List]");
		return vfCharacterService.getAllList();
	}
	
	@ResponseBody
	@GetMapping(path = "/dailyCharacter")
	public VFCharacterResponseVo getDailyCharacter() throws IOException {
		return vfCharacterService.getDailyCharacter();
	}
}
