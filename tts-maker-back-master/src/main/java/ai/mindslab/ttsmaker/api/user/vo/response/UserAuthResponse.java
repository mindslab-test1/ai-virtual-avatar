package ai.mindslab.ttsmaker.api.user.vo.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class UserAuthResponse {
	private Long userNo;
	private String message;
}
