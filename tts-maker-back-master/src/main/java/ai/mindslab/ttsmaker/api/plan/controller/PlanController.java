package ai.mindslab.ttsmaker.api.plan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.plan.service.PlanService;
import ai.mindslab.ttsmaker.api.plan.vo.response.PlanListResponseVo;

@RestController
@RequestMapping("/plan")
public class PlanController {
	
	@Autowired
	private PlanService planService;
	
	/***
	 * <pre>
	 * 구독 상품 리스트 조회
	 * </pre>
	 * @return
	 */
	@ResponseBody
	@GetMapping("/list")
    public List<PlanListResponseVo> getList() {
		return planService.getList();
	}

}
