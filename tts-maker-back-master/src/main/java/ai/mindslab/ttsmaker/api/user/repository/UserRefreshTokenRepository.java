package ai.mindslab.ttsmaker.api.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.ttsmaker.api.user.domain.UserRefreshToken;

@Repository
public interface UserRefreshTokenRepository extends JpaRepository<UserRefreshToken, Long> {
	Optional<UserRefreshToken> findByRefreshToken(String refreshToken);
	
    @Transactional
    @Modifying
    @Query("update UserRefreshToken t set t.active = false where t.refreshToken = :refreshToken")
	void setRefreshTokenBlocklist(@Param("refreshToken") String refreshToken);
}
