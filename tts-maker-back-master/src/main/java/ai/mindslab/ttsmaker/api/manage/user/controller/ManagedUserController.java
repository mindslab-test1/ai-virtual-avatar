package ai.mindslab.ttsmaker.api.manage.user.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.manage.user.service.ManagedUserService;
import ai.mindslab.ttsmaker.api.manage.user.vo.reponse.UserListReponseDto;
import ai.mindslab.ttsmaker.api.manage.user.vo.request.UserSearchRequestDto;
import ai.mindslab.ttsmaker.common.page.AvaPage;
import ai.mindslab.ttsmaker.common.page.PageRequest;


@RestController
@RequestMapping(path = "/manage/users")
public class ManagedUserController {

	@Autowired
	private ManagedUserService managedUserService;
	
    @GetMapping
    @ResponseBody
    public AvaPage<UserListReponseDto> getUserList(final PageRequest pageable, final UserSearchRequestDto searchRequestDto) {
		pageable.setSortColumn("id");
		return managedUserService.getList(pageable.of(), searchRequestDto);
	}

}
