package ai.mindslab.ttsmaker.api.user.service;

import java.sql.Date;

import ai.mindslab.ttsmaker.api.user.vo.request.UserSignUpRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserSignUpResponseVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserVo;

public interface UserService {
	public UserVo getUserVoFindByUserId(String userId);

	public UserSignUpResponseVo userSignUp(UserSignUpRequestVo userSignUpRequestVo);
}
