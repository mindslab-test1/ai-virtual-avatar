package ai.mindslab.ttsmaker.api.job.service;

import java.util.List;

import ai.mindslab.ttsmaker.api.job.vo.request.JobContentSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.request.JobScriptSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobContentVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobListVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobScriptVo;

public interface JobService {
	/**
	 * <pre>
	 * JOB List 조회
	 * </pre>
	 * @return
	 */
	public List<JobListVo> getJobList();

	/**
	 * <pre>
	 * 각 User ID마다 JOB List 조회
	 * </pre>
	 *
	 * @return
	 */
	public List<JobListVo> getJobListByUserNo(Long userNo);
	
	/**
	 * <pre>
	 * JOB ID별 선택한 화자 및 스크립트 로드
	 * </pre>
	 * @param jobId
	 * @return
	 */
	public JobContentVo getJobContent(String jobId);
	
	/**
	 * <pre>
	 * Job ID별 스크립트 LIST 조회
	 * </pre>
	 * @param jobId
	 * @return
	 */
	public List<JobScriptVo> getJobScriptList(String jobId);
	
	/**
	 * <pre>
	 * script 조회(Job ID + Script ID)
	 * </pre>
	 * @param jobId
	 * @param scriptId
	 * @return
	 */
	public JobScriptVo getJobScript(String jobId, String scriptId);

	/**
	 * <pre>
	 * 스크립트(JobScriptSaveVo) 저장(Redis)
	 * </pre>
	 * @param jobId
	 * @param scriptId
	 * @param saveVo
	 * @return
	 */
	public boolean setJobScriptsSaveVoTagetByRedis(String jobId, String scriptId, JobScriptSaveVo saveVo);
	

	/**
	 * <pre>
	 * JOB 저장
	 * </pre>
	 * @param jobId
	 * @param jobContentSaveVo
	 * @return
	 */
	public boolean saveJobContent(String jobId, JobContentSaveVo jobContentSaveVo);
}
