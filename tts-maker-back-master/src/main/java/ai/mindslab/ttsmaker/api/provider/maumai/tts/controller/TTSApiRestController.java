package ai.mindslab.ttsmaker.api.provider.maumai.tts.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.provider.maumai.tts.service.TTSApiService;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.MaumAIResponseFile;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/maumAi/tts")
public class TTSApiRestController {

	@Autowired
	private TTSApiService maumAiApiService;

	@ResponseBody
	@PostMapping(value = "/requestMakeFile/{voiceName}")
	public Mono<MaumAIResponseFile> requestMakeFile(@PathVariable String voiceName, @RequestParam String text) throws IOException, InterruptedException {
		log.info("[Make TTS File] : {}, {}", voiceName, text);
		return maumAiApiService.requestTTSMakeFile(voiceName, text, null);
	}
}
