package ai.mindslab.ttsmaker.api.user.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.user.service.UserService;
import ai.mindslab.ttsmaker.api.user.vo.request.UserSignUpRequestVo;
import ai.mindslab.ttsmaker.api.user.vo.response.UserSignUpResponseVo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserRestController {
	
	@Autowired
	private UserService userService;

	@ResponseBody
	@PostMapping(path = "/sign_up")
	public UserSignUpResponseVo userSignUp(@RequestBody UserSignUpRequestVo userSignUpRequestVo) throws IOException {
		log.info("[UserSignUpRequestVo] : {}", userSignUpRequestVo);
		return userService.userSignUp(userSignUpRequestVo);
	}
}
