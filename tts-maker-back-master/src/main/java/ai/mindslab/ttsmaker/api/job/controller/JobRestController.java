package ai.mindslab.ttsmaker.api.job.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.ttsmaker.api.job.service.JobService;
import ai.mindslab.ttsmaker.api.job.vo.request.JobContentSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.request.JobScriptSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobContentVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobListVo;
import ai.mindslab.ttsmaker.api.script.domain.dto.response.TTSMakeResultVo;
import ai.mindslab.ttsmaker.api.script.service.TTSMakeService;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUser;
import ai.mindslab.ttsmaker.configurers.resolvers.AvaUserParam;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/job")
public class JobRestController {

	@Autowired
	private JobService jobService;
	
	@Autowired
	private TTSMakeService ttsService;
	
	@ResponseBody
	@GetMapping(value = "/myJobList")
	public List<JobListVo> myJobList(@AvaUserParam AvaUser avaUser) throws IOException {
		log.info("[myJobList By User] : {}", AvaUser.userPrint(avaUser));
		return jobService.getJobListByUserNo(avaUser.getUserNo());
	}
	
	@ResponseBody
	@GetMapping(value = "/{jobId}")
	public JobContentVo getJobContent(@PathVariable String jobId) throws IOException {
		log.info("[Getting Job Content] : {}", jobId);
		return jobService.getJobContent(jobId);
	}
	
	@ResponseBody
	@PutMapping(value = "/{jobId}")
	public boolean saveJobContent(@AvaUserParam AvaUser avaUser, @PathVariable String jobId, @RequestBody JobContentSaveVo jobContentSaveVo) {
		log.info("[Saving Job Content] : {}", jobId, jobContentSaveVo);
		return jobService.saveJobContent(jobId, jobContentSaveVo);
	}
	
	@ResponseBody
	@PutMapping(value = "/{jobId}/{scriptId}")
	public boolean updateJobScript(@PathVariable String jobId, @PathVariable String scriptId, @RequestBody JobScriptSaveVo saveVo) {
		log.info("[Setting Job Script] : {}, {}", jobId, scriptId);
		return jobService.setJobScriptsSaveVoTagetByRedis(jobId, scriptId, saveVo);
	}
	
	@ResponseBody
	@PostMapping(value = "/requestTTS/{characterId}")
	public Mono<TTSMakeResultVo> requestTTS(@PathVariable String characterId, @RequestParam String text) throws IOException, InterruptedException {
		log.info("[Requesting toMakeTTSv2] : {}, {}", characterId, text );
		return ttsService.requestTTSMakeFile(characterId, text);
	}
}
