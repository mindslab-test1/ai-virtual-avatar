package ai.mindslab.ttsmaker.api.job.vo.request;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobContentSaveVo {
	private String jobTitle;								// 작업 제목
	private List<JobScriptSaveVo> scripts;				// 작성한 스크립트 리스트
	private List<JobScriptSaveVo> deletedScripts;		// 삭제할 스크립트 리스트
	private Long userNo;
}
