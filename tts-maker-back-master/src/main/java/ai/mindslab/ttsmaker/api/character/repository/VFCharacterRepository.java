package ai.mindslab.ttsmaker.api.character.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;

public interface VFCharacterRepository extends JpaRepository<VFCharacter, Long> { 
	
	@Query("select p from VFCharacter p where p.isDeleted = 'N'")
	List<VFCharacter> findActiveList();
}
