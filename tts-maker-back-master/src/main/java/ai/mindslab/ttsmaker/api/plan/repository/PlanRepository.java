package ai.mindslab.ttsmaker.api.plan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ai.mindslab.ttsmaker.api.plan.domain.Plan;

@Repository
public interface PlanRepository extends JpaRepository<Plan, Integer> {
    @Query("select p from Plan p where p.exposeYn = 'Y'")
    List<Plan> findPlanListByExposed();
}
