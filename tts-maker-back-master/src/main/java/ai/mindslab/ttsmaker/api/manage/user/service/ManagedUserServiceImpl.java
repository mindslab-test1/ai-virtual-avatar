package ai.mindslab.ttsmaker.api.manage.user.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import ai.mindslab.ttsmaker.api.manage.user.service.conditions.UserSearchSpecs;
import ai.mindslab.ttsmaker.api.manage.user.vo.reponse.UserListReponseDto;
import ai.mindslab.ttsmaker.api.manage.user.vo.request.UserSearchRequestDto;
import ai.mindslab.ttsmaker.api.user.domain.UserM;
import ai.mindslab.ttsmaker.api.user.repository.UserMRepository;
import ai.mindslab.ttsmaker.common.page.AcaPageImpl;
import ai.mindslab.ttsmaker.common.page.AvaPage;

@Service
public class ManagedUserServiceImpl implements ManagedUserService {

	@Autowired
	private UserMRepository userMRepository;
	
	@Override
	public AvaPage<UserListReponseDto> getList(PageRequest pageable, UserSearchRequestDto searchDto) {
		Specification<UserM> specification  = UserSearchSpecs.chainingSpecs(searchDto);
		Page<UserM> page = userMRepository.findAll(specification, pageable);
		List<UserListReponseDto> list = page.getContent().stream().map(UserListReponseDto::of).collect(Collectors.toList());
		return new AcaPageImpl<>(list, pageable, page.getTotalElements());
	}
}
