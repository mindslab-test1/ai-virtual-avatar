package ai.mindslab.ttsmaker.api.plan.service;

import java.util.List;

import ai.mindslab.ttsmaker.api.plan.vo.response.PlanListResponseVo;

public interface PlanService {

	/**
	 * <pre>
	 * 플랜 상품 리스트 조회
	 * </pre>
	 * @return
	 */
	List<PlanListResponseVo> getList();

}
