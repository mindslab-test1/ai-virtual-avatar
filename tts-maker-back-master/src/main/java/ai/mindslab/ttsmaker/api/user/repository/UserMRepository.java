package ai.mindslab.ttsmaker.api.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ai.mindslab.ttsmaker.api.user.domain.UserM;

@Repository
public interface UserMRepository extends JpaRepository<UserM, Long>, JpaSpecificationExecutor<UserM> {
	
	Optional<UserM> findByUserId(String userId);

}
