package ai.mindslab.ttsmaker.api.user.service;

import java.util.Date;

public interface AuthService {
	
	public boolean saveRefreshToken(Long userNo, String refreshToken, Date refreshTokenExpireDate);
	public boolean setRefreshTokenBlocklist(Long userNo, String refreshToken);

}
