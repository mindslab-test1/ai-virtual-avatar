package ai.mindslab.ttsmaker.api.provider.maumai.tts.exception.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.exception.MaumAIApiException;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.MaumAIResponseError;

import java.util.UUID;

@ControllerAdvice
public class MaumAIApiRestExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(value = { MaumAIApiException.class })
	protected ResponseEntity<Object> handleConflict(MaumAIApiException ex, WebRequest request) {
		HttpStatus statusCode = ex.getStatusCode();
		MaumAIResponseError error = MaumAIResponseError.builder()
			.requestId(UUID.randomUUID().toString())
			.httpStatus(ex.getStatusCode().value())
			.message(String.format("TTS 처리 중 오류(%s:%s)가 발생했습니다.", statusCode.value(), statusCode.name()))
			.errorCode(ex.getErrorCode())
			.errorMessage(ex.getErrorMessage()).build();
		
		return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.OK, request);
	}
}
