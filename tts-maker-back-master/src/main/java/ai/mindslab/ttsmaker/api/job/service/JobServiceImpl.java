package ai.mindslab.ttsmaker.api.job.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;

import ai.mindslab.ttsmaker.api.character.domain.VFCharacter;
import ai.mindslab.ttsmaker.api.character.service.VFCharacterService;
import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.job.domain.JobM;
import ai.mindslab.ttsmaker.api.job.domain.JobScript;
import ai.mindslab.ttsmaker.api.job.repository.JobMRepository;
import ai.mindslab.ttsmaker.api.job.repository.JobScriptRepository;
import ai.mindslab.ttsmaker.api.job.vo.request.JobContentSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.request.JobScriptSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobContentVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobListVo;
import ai.mindslab.ttsmaker.api.job.vo.response.JobScriptVo;
import ai.mindslab.ttsmaker.util.DateUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class JobServiceImpl implements JobService {

    @PersistenceContext
    private EntityManager em;
    
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    
    @Autowired
    private JobMRepository jobMRepository;

    @Autowired
    private JobScriptRepository jobScriptRepository;
    
    @Autowired
    private VFCharacterService vfCharacterService;  
    

    @Override
    public List<JobListVo> getJobList() {
        try {
            List<JobM> list = jobMRepository.getActiveJobList(Sort.by("orderSeq"));
            return list.stream().map(JobListVo::of).collect(Collectors.toList());
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0001", "작업 리스트 조회 중 오류가 발생했습니다.");
        }
    }

    @Override
    public List<JobListVo> getJobListByUserNo(Long userNo) {
        try {
            List<JobM> list = jobMRepository.getActiveJobListByUserId(userNo, Sort.by("orderSeq"));
            return list.stream().map(JobListVo::of).collect(Collectors.toList());
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0001", "작업 리스트 조회 중 오류가 발생했습니다.");
        }
    }

    @Override
    public JobContentVo getJobContent(String jobId) {
        try {
            JobM jobM = jobMRepository.getJobContentFindByJobId(jobId);
            List<JobScriptVo> jobScriptVoList = Lists.newArrayList();
            if (jobM != null) {
                List<JobScript> jobScripts = jobM.getJobScripts();
                if (!CollectionUtils.isEmpty(jobScripts)) {
                    jobScriptVoList = makeJobScriptVoList(jobScripts);
                    setJobScriptsSaveVoMapTagetByRedis(jobId, makeJobScriptSaveVoMap(jobScripts));
                }
                return JobContentVo.of(jobM, jobScriptVoList);
            }
            return new JobContentVo();
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0002", "작업 데이터를 불러오는중  오류가 발생했습니다.");
        }
    }

    @Override
    public List<JobScriptVo> getJobScriptList(String jobId) {
        try {
            List<JobScript> list = jobScriptRepository.getActiveScriptListFindByJobId(jobId, Sort.by("orderSeq"));
            return list.stream().map(JobScriptVo::of).collect(Collectors.toList());
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0003", "Script List 데이터를 불러오는중 오류가 발생했습니다.");
        }
    }

    @Override
    public JobScriptVo getJobScript(String jobId, String scriptId) {
        try {
            return JobScriptVo.of(jobScriptRepository.getJobScriptFindByJobIdAndScriptId(jobId, scriptId));
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0004", "Script 데이터를 불러오는중 오류가 발생했습니다.");
        }
    }

    /**
     * <pre>
     * jobScriptsSaveVoMap 저장(Redis), JobContent 조회시 저장
     * </pre>
     *
     * @param jobId
     * @param jobScriptsSaveVoMap
     * @return
     */
    public boolean setJobScriptsSaveVoMapTagetByRedis(String jobId, Map<String, JobScriptSaveVo> jobScriptsSaveVoMap) {
        try {
            String key = String.format("%s:scripts", jobId);
            redisTemplate.opsForHash().putAll(key, jobScriptsSaveVoMap);
            redisTemplate.expireAt(key, DateUtils.asDate(DateUtils.localDateTimeNow().plusDays(2)));
            return true;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0008", "JobScriptSaveVo Map 업데이트 처리 중 오류가 발생했습니다.");
        }
    }


    /**
     * <pre>
     * JobScriptSaveVo 저장(Redis)
     * </pre>
     *
     * @param jobId
     * @param scriptId
     * @param saveVo
     * @return
     */
    @Override
    public boolean setJobScriptsSaveVoTagetByRedis(String jobId, String scriptId, JobScriptSaveVo saveVo) {
        try {
            String key = String.format("%s:scripts", jobId);
            redisTemplate.opsForHash().put(key, scriptId, saveVo);
            redisTemplate.expireAt(key, DateUtils.asDate(DateUtils.localDateTimeNow().plusDays(2)));
            return true;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0009", "JobScriptSaveVo 업데이트 처리 중 오류가 발생했습니다.");
        }
    }

    /**
     * <pre>
     * JobScriptSaveVo 조회(Redis), JobContent 저장시 사용
     * </pre>
     *
     * @param jobId
     * @param scriptId
     * @return
     */
    public JobScriptSaveVo getJobScriptSaveVoFromRedis(String jobId, String scriptId) {
        try {
            String key = String.format("%s:scripts", jobId);
            JobScriptSaveVo saveScript = (JobScriptSaveVo) redisTemplate.opsForHash().get(key, scriptId);
            return saveScript;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ00010", "JobScriptSaveVo 조회중 오류가 발생했습니다.");
        }
    }

    @Transactional
    @Override
    public boolean saveJobContent(String jobId, JobContentSaveVo jobContentSaveVo) {
        log.info("Saving {} jobContent  ==> {}", jobId, jobContentSaveVo);

        try {
            List<JobScript> jobScripts = Lists.newArrayList();
            // JOB 레코드 저장
            JobM jobM = jobMRepository.getJobContentFindByJobId(jobId);
            if (jobM != null) {
                jobM.setJobTitle(jobContentSaveVo.getJobTitle());
                jobM.setUpdatedAt(DateUtils.localDateTimeNow());
                em.merge(jobM);

                jobScripts = jobM.getJobScripts();
            } else {
                jobM = new JobM();
                jobM.setJobId(jobId);
                jobM.setUserNo(jobContentSaveVo.getUserNo());
                jobM.setJobTitle(jobContentSaveVo.getJobTitle());
                jobM.setCreatedAt(DateUtils.localDateTimeNow());
                jobM.setUpdatedAt(DateUtils.localDateTimeNow());
                em.merge(jobM);
            }
            Map<String, VFCharacter> charactersMap = makeVFCharactersMap(); 

            Map<String, JobScript> jobScriptsMap = makeJobScriptMap(jobScripts);

            // 기존 스크립트 삭제
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getDeletedScripts())) {
                for (JobScriptSaveVo deletedScript : jobContentSaveVo.getDeletedScripts()) {
                    if (jobScriptsMap.containsKey(deletedScript.getScriptId())) {
                        JobScript jobScript = jobScriptsMap.get(deletedScript.getScriptId());
                        jobScript.setIsDeleted("Y");
                        em.merge(jobScript);
                    }
                }
            }
            // 스크립트 저장/수정
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getScripts())) {
                long orderSeq = 1L;
                for (JobScriptSaveVo saveScript : jobContentSaveVo.getScripts()) {
                    boolean existDbJobScript = jobScriptsMap.containsKey(saveScript.getScriptId());

                    JobScriptSaveVo jobScriptTemp = getJobScriptSaveVoFromRedis(jobId, saveScript.getScriptId());
                    saveScript = Optional.ofNullable(jobScriptTemp).orElse(saveScript);
                    VFCharacter character = charactersMap.get(saveScript.getCharacterId());
                    JobScript jobScript = null;
                    if (existDbJobScript) {
                        jobScript = jobScriptsMap.get(saveScript.getScriptId());
                        jobScript = adjUpdateJobScript(existDbJobScript, jobScript, character, saveScript, orderSeq);
                    } else {
                        jobScript = new JobScript();
                        jobScript.setJobId(jobId);
                        jobScript = adjUpdateJobScript(existDbJobScript, jobScript, character, saveScript, orderSeq);
                    }
                    em.merge(jobScript);
                    setJobScriptsSaveVoTagetByRedis(jobId, saveScript.getScriptId(), JobScriptSaveVo.of(jobScript));
                    orderSeq++;
                }
            }

            jobScriptRepository.deleteByIsDeleted("Y");

            em.flush();
            return true;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0007", "JOB 저장 처리 중 오류가 발생했습니다. : " + ex.getMessage());
        }
    }

	/**
     * <pre>
     * JobScript 데이터 가공(신규/업데이트)
     * </pre>
     *
     * @param existJobScript
     * @param jobScript
     * @param character
     * @param saveVo
     * @param orderSeq
     * @return
     */
    private JobScript adjUpdateJobScript(boolean existJobScript, JobScript jobScript, VFCharacter character, JobScriptSaveVo saveVo, long orderSeq) {
        log.info("Updating JobScript for {}", jobScript.getJobId());

        if (existJobScript) {
            jobScript.setScriptText(saveVo.getScriptText());                                // TTS 스크립트 Text
            jobScript.setTtsUrl(saveVo.getTtsUrl());                    // TTS Resource URL정보
            jobScript.setTtsExpiredDate(saveVo.getTtsExpiredDate());    // TTS Resource 만료일
            jobScript.setOrderSeq(orderSeq);                                                // 정렬순서
            jobScript.setTtsState(saveVo.getTtsState());                                    // TTS 상태
            jobScript.setRowState("S");                                                        // ROW 상태
            jobScript.setCreatedAt(DateUtils.localDateTimeNow());
            jobScript.setUpdatedAt(DateUtils.localDateTimeNow());
        } else {
            jobScript.setScriptId(saveVo.getScriptId());
            jobScript.setScriptText(saveVo.getScriptText());                                // TTS 스크립트 Text
            jobScript.setVfCharacter(character);                                                    // 화자 정보
            jobScript.setTtsUrl(saveVo.getTtsUrl());                    // TTS Resource URL정보
            jobScript.setTtsExpiredDate(saveVo.getTtsExpiredDate());    // TTS Resource 만료일
            jobScript.setOrderSeq(orderSeq);                                                // 정렬순서
            jobScript.setTtsState(saveVo.getTtsState());                                    // TTS 상태
            jobScript.setRowState("S");                                                        // ROW 상태
            jobScript.setCreatedAt(DateUtils.localDateTimeNow());
        }
        return jobScript;
    }
    
    private Map<String, VFCharacter> makeVFCharactersMap() {
		return vfCharacterService.allEntities().stream().collect(Collectors.toMap(VFCharacter::getCharacterId, Function.identity()));
	}

    /**
     * <pre>
     * List<JobScript> to Map<String, JobScript> 처리
     * </pre>
     *
     * @param jobScripts
     * @return
     */
    private Map<String, JobScript> makeJobScriptMap(List<JobScript> jobScripts) {
        log.info("Mapping JobScript {}", jobScripts);
        if (!CollectionUtils.isEmpty(jobScripts)) {
            return jobScripts.stream().collect(Collectors.toMap(JobScript::getScriptId, Function.identity()));
        }
        return new HashMap<>();
    }

    /**
     * <pre>
     * List<JobScript> to List<JobScriptVo> 처리
     * </pre>
     *
     * @param jobScripts
     * @return
     */
    private List<JobScriptVo> makeJobScriptVoList(List<JobScript> jobScripts) {
        log.info("Making JobScript list {}", jobScripts);

        if (!CollectionUtils.isEmpty(jobScripts)) {
            return jobScripts.stream().map(JobScriptVo::of).collect(Collectors.toList());
        }
        return Lists.newArrayList();
    }

    /**
     * <pre>
     * List<JobScript> to Map<String, JobScriptSaveVo> 처리
     * </pre>
     *
     * @param jobScripts
     * @return
     */
    private Map<String, JobScriptSaveVo> makeJobScriptSaveVoMap(List<JobScript> jobScripts) {
        if (!CollectionUtils.isEmpty(jobScripts)) {
            return jobScripts.stream().map(JobScriptSaveVo::of).collect(Collectors.toMap(JobScriptSaveVo::getScriptId, Function.identity()));
        }
        return new HashMap<>();
    }

    
}
