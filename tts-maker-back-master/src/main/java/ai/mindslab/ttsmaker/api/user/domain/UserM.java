package ai.mindslab.ttsmaker.api.user.domain;


import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@DynamicInsert 
@DynamicUpdate 
@Getter
@Setter
@Entity
@Table(name = "USER_M", uniqueConstraints = {@UniqueConstraint(name = "UK_USER_ID", columnNames = {"USER_ID"})})
public class UserM {

    //관리자번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO", columnDefinition = "int(11)")
    private Long userNo;

    //user ID
    @Column(name = "USER_ID", columnDefinition = "varchar(100)")
    private String userId;

    //user PW
    @Column(name = "USER_PW", columnDefinition = "varchar(100)")
    private String userPw;

    //이름
    @Column(name = "USER_NM", columnDefinition = "varchar(100)")
    private String userNm;

    //회사
    @Column(name = "COMPANY_NM", columnDefinition = "varchar(20)")
    private String companyNm;

    //전화번호
    @Column(name = "CELL_PHONE", columnDefinition = "varchar(11)")
    private String cellphone;

    @Column(name = "ACTIVE", columnDefinition = "boolean default true")
    private Boolean active;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "USER_ROLES", joinColumns = @JoinColumn(name = "USER_NO"), inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
    private Collection<Role> roles;
    
  //등록일시
    @Column(name = "CREATE_DATE")
    @CreationTimestamp
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createDate;

    //수정일시
    @Column(name = "UPDATE_DATE")
    @UpdateTimestamp
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedDate;

}
