package ai.mindslab.ttsmaker.util;

public class StringUtils {
	public static String nvl(String str, String defaultStr) {
		return str == null ? defaultStr : str;
	}

	public static Integer toInt(String value) {
		try {
			return Integer.parseInt(value);
		} catch (Exception e) {
			return 0;
		}
	}

	public static Long toLong(String value) {
		try {
			return Long.parseLong(value);
		} catch (Exception e) {
			return 0l;
		}
	}
}
