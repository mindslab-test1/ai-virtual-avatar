import { RepositoryFactory } from '@/repositories/RepositoryFactory';
const userRepository = RepositoryFactory.get('user');


export default {
    ACCESS_TOKEN_KEY = 'access_token',
    REFRESH_TOKEN_KEY = 'refresh_token',

    getRefreshToken = () => {
        return localStorage.getItem(this.REFRESH_TOKEN_KEY);
    },

    getRefreshToken = () => {
        return new Promise((resolve, reject) => {
            let refreshToken = this.getRefreshToken() || '';
            if(refreshToken) {
                userRepository.getAccessTokenFromRefreshToken({ 'refreshToken' : refreshToken }).then(response =>{
                    resolve(response.data.accessToken);
                }).catch((error) => {
                    reject(error);
                });
            } else {
                reject({cause : 'refreshToken is empty'});
            }
        });
    },

}