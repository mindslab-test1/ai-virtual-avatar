import Repository from "./Repository";

const resource = "/plan";

export default {
    getList() {
      return Repository.get(`${resource}/list`);
    },
};