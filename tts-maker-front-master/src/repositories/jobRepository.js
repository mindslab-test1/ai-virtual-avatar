import Repository from "./Repository";

const resource = "/job";
export default {
    myJobList() {
        return Repository.get(`${resource}/myJobList`);
    },
    getJobContent(jobId) {
        return Repository.get(`${resource}/${jobId}`);
    },
    saveJobContent(jobId, jobContent) {
        return Repository.put(`${resource}/${jobId}`, jobContent);
    },
    updateJobScript(jobId, jobScriptId, jobScript) {
        return Repository.put(`${resource}/${jobId}/${jobScriptId}`, jobScript);
    },
    requestTTS(characterId, text) {
        var params = new URLSearchParams();
        params.append('text', text);
        return Repository.post(`${resource}/requestTTS/${characterId}`, params);
    }
};