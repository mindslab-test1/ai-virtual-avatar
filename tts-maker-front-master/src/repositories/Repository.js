import axios from "axios";

const baseURL = process.env.API_BASE_URL;

const instance = axios.create({
  baseURL
});

instance.defaults.timeout = 30000;

axios.interceptors.request.use((config) => {

  let access_token = localStorage.getItem('access_token') || '';
  if(access_token) {
    config.headers.Authorization = `Bearer ${access_token}`;
  }

  return config
}, (error) => {
  // Do something with request error
  return Promise.reject(error)
})

instance.interceptors.response.use( (response) => {
  return response;
}, (error) => {
  if (error.response.status === 401) {
    if(error.config.url == '/user/sign_in' || error.config.url == '/user/sign_out') {
      return Promise.resolve(error.response)
    }

    if (error.config.url == '/auth/refreshToken' || error.response.message == 'Account is disabled.') {
      TokenStorage.clear();
      router.push({ name: 'UserSignInView' });

      return new Promise((resolve, reject) => {
        reject(error);
      });
    }
  }

  return new Promise((resolve, reject) => {
    reject(error);
  });
});

export default instance; 