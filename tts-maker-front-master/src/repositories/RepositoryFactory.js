import CharacterRepository from './characterRepository';
import JobRepository from "./jobRepository";
import PlanRepository from './PlanRepository';
import UserRepository from './userRepository';

const repositories = {
  character: CharacterRepository,
  job: JobRepository,
  plan: PlanRepository,
  user: UserRepository,
  // other repositories ...
};

export const RepositoryFactory = {
  get: name => repositories[name]
};
  