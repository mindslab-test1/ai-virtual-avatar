// JOB Script getter(index)
const getRefreshTokenFromLocalStorage = () => ()  =>{
    return localStorage.getItem("refresh_token") || '';
};

export default {
    getRefreshTokenFromLocalStorage,
};