const setUserInfo = (state, userInfo) => {
    state.userInfo = userInfo || {}
    localStorage.setItem("user", JSON.stringify(state.userInfo));
};

const setUserToken = (state, {access_token, refresh_token}) => {
    localStorage.setItem("access_token", access_token);
    localStorage.setItem("refresh_token", refresh_token);
};

const clearUserInfo = (state) => {
    localStorage.removeItem("access_token");
    localStorage.removeItem("refresh_token");
    localStorage.removeItem("user");
    state.userInfo = {};
};



export default {
    setUserInfo,
    setUserToken,
    clearUserInfo,
};