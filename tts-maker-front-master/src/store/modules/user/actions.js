import 'url-search-params-polyfill';
import Repository from '@/repositories/Repository'
import { RepositoryFactory } from '@/repositories/RepositoryFactory';
const userRepository = RepositoryFactory.get('user');

const signIn =  ({ commit, state }, loginObj) => { 
    var params = new URLSearchParams();
    params.append('userId', loginObj.userId);
    params.append('userPw', loginObj.userPw);
    
    return new Promise((resolve, reject) => {
        userRepository.sign_in(params).then((res) => {
            if(res.status == 200) {
                let userInfo = res.data;
                commit('setUserToken', {'accessToken' : userInfo['accessToken'], 'refresh_token': userInfo['refreshToken'] });
                delete userInfo['access_token']
                delete userInfo['refresh_token']
                commit('setUserInfo', userInfo)
                return resolve({ loggedIn : true});
            }else if(res.status == 401) {
                return reject({ loggedIn : false, reason: res.data.errorKorMessage});
            }else {
                return reject({ loggedIn : false, reason: "시스템 오류 발생" });
            }
        });
    });
};

const signOut =  ({ commit, state, getters }) => {

    let userNo = state.userInfo.userNo;
    let refresh_token = getters.getRefreshTokenFromLocalStorage();
    
    return new Promise((resolve, reject) => {
        if(!refresh_token) {
            commit('clearUserInfo')
            return resolve({ loggedOut : true});
        }else {
            let params = { 'userNo': userNo, 'refreshToken' : refresh_token };
            userRepository.sign_out(params).then((res) => {
                commit('clearUserInfo')
                return resolve({ loggedOut : true, message: res.data.message });
            }).catch((res) => {
                return reject({ loggedOut : false, reason: "로그아웃 실패"});
            });
        }
        
    });
    
};

export default {
    signIn,
    signOut,
};