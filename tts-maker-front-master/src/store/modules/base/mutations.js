const setModal = (state, modalMap) => {
    state.modalState = modalMap || {}
};

export default {
    setModal,
};