const setModal = ({ commit, state }, params ) => {
    let targetKeys = [];
    if(typeof(params) == 'object' && params._isVue) {
        targetKeys.push(this.$options.name);
    }else if((typeof(params) == 'object') && Array.isArray(params)) {
        for(let key in params) {
            targetKeys.push(key);
        }
    }else {
        targetKeys.push(params)
    }

    let targetModal = {};
    targetKeys.forEach(targetKey => {
        targetModal[targetKey] = true;
    });
    commit('setModal', targetModal)
};

const removeModal = ({ commit, state }, params ) => {
    let targetKeys = [];
    if(typeof(params) == 'object' && params._isVue) {
        targetKeys.push(params.$options.name);
    }else if((typeof(params) == 'object') && Array.isArray(params)) {
        for(let key in params) {
            targetKeys.push(key);
        }
    }else {
        targetKeys.push(params)
    }

    let targetModal = Object.assign({}, state.modalState);
    targetKeys.forEach(targetKey => {
        if(targetModal.hasOwnProperty(targetKey)) {
            delete targetModal[targetKey]
        }
    });
    commit('setModal', targetModal)
};

const clearModal =  ({ commit, state }) => {
    commit('setModal', {})
};

export default {
    setModal,
    removeModal,
    clearModal,
};