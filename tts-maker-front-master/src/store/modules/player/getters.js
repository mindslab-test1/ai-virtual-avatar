const findConvertableJobScriptIndex = (state, getters, rootState) => (index) => {
    index = index || 0;
    let candIndex = index;
    let jobScripts = rootState.job.jobScripts;
    jobScripts.slice(index).some(jobScript=> {
        if(jobScript.scriptText.length > 0) {
            return true;
        }
        candIndex++;
    })

    return candIndex < jobScripts.length ? candIndex : -1;
};

const isConverting = (state, getters, rootState, rootGetters) => (index) => {
    let jobScript = rootGetters["job/getJobScriptFindByIndex"](index)
    return jobScript.ttsState === 'C';
};
const isPlayEnable = (state, getters, rootState, rootGetters) => (index) => {
    let jobScript = rootGetters["job/getJobScriptFindByIndex"](index)
    return jobScript.ttsState === 'S';
};

const getIsPlaying = (state, getters, rootState, rootGetters) => (index) => {
    return state.cursor == index;
    
};





const getCursor = ( state ) => {
    return state.cursor;
};



export default {
    findConvertableJobScriptIndex,
    isConverting,
    isPlayEnable,
    getIsPlaying,
    getCursor,
};