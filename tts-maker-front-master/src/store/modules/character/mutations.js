/**
 * Speaker List 저장
 * @param {*} characters 
 */
const setCharacters = (state, characters) => {
    state.characters = characters || [];
    characters.forEach(character=>state.characterMap[character.characterId] = character)
};

const setLastCharacter = (state, character) => {
    state.last_character = character;
    state.last_character_id = character.characterId;
};

const showModal = (state, modal) => {
    state.modalState = modal
};



export default {
    setCharacters,
    setLastCharacter,
    showModal,
};