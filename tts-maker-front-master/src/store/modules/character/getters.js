const getCharacter = (state) => (characterId) => {
    return state.characters.find(character => character.speakerId === characterId);
};
export default {
    getCharacter,
};