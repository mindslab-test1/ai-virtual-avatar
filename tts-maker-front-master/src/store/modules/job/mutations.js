const setJobList = (state, setJobList) => {
    state.jobList = setJobList || [];
};

/**
 * 신규 스크립트 생성 처리(alt+enter) - Action callee
 * @param {*} index 신규생성할 인덱스 위치
 * @param {*} script 신규 생성될 스크립트
 */
const insertAtJobScript = (state, { index, script }) => {
    state.jobScripts.splice(index, 0, script);
};

/**
 * 스크립트 삭제 처리 - Action callee
 * @param {*} 삭제될 인덱스 위치 
 * @param {*} 삭제할 스크립트
 */
const removeAtJobScript = (state, { index }) => {
    if ( state.jobScripts.length ) {
        state.jobScripts.splice(index, 1);
    }
};

const pushDeletedJobScript = (state, {index}) => {
    state.deletedJobScripts.push(Object.assign({}, state.jobScripts[index], { 'rowState' : 'D' }));
};

/**
 * 변경 텍스트 업데이트 처리 - Action callee
 * @param {*} index 변경하고자하는 스크립트 인덱스
 * @param {*} scriptText 변경하고자하는 텍스트
 * @param {*} rowState 변경하고자하는 스크립트 ROW 상태
 */ 
const updateAtJobScriptText = (state, { index, scriptText, rowState }) => {
    Object.assign(state.jobScripts[index], { 'scriptText' : scriptText, 'rowState' : rowState });
};

const updateAtJobScriptFromPayLoad = (state, payload) => {
    let index = payload.index;
    delete payload['index'];
    Object.assign(state.jobScripts[index], payload);
    return state.jobScripts[index];
};

const updateAtJobScriptTTSReponse = (state, { index, ttsState = 'W', ttsUrl, ttsExpiredDate}) => {
    if(state.jobScripts[index].hasOwnProperty("ttsUrl")) {
        delete state.jobScripts[index]['ttsUrl']
    }

    if(state.jobScripts[index].hasOwnProperty("ttsExpiredDate")) {
        delete state.jobScripts[index]['ttsExpiredDate']
    }
    let updateReponse = {'ttsState' : ttsState } 
    if(ttsUrl) {
        updateReponse['ttsUrl'] = ttsUrl
    }
    if(ttsExpiredDate) {
        updateReponse['ttsExpiredDate'] = ttsExpiredDate
    }

    Object.assign(state.jobScripts[index], updateReponse);
};

const updateJobScriptFindByScriptId = (state, payload) => {
    const searchKey = payload.scriptId;
    const index = state.jobScripts.findIndex(script => script.scriptId === searchKey);
    Object.assign(state.jobScripts[index], payload);
    return state.jobScripts[index];
};

/**
 * JOB ID 저장
 * @param {*} jobId JOB ID
 */
const setJobId = (state, jobId) => {
    state.jobId = jobId;
}

/**
 * JOB TITLE 저장
 * @param {*} jobId JOB TITLE
 */
const setJobTitle = (state, jobTitle) => {
    state.jobTitle = jobTitle;
}


const clearJobTitle = (state) => {
    state.jobTitle = '';
};

const setJobScripts = (state, jobScripts) => {
    state.deletedJobScripts = [];
    state.jobScripts = jobScripts || [];
};

/**
 * JOB Script Delete candidate Row 초기화 처리 - Action calle
 */
const clearDeletedJobScripts = (state) => {
    state.deletedJobScripts = [];
}

/**
 * 변경 텍스트 업데이트 처리 - Action callee
 * @param {*} index 변경하고자하는 스크립트 인덱스
 * @param {*} scriptText 변경하고자하는 텍스트
 * @param {*} rowState 변경하고자하는 스크립트 ROW 상태
 */ 
const updateAtScriptText = (state, { index, scriptText, rowState }) => {
    Object.assign(state.jobScripts[index], { 'scriptText' : scriptText, 'rowState' : rowState, 'ttsState' : 'W', 'ttsResourceExpiryDate' : null, 'ttsResourceUrl' : null });
};

/**
 * 기존 스크립트 화자 업데이트 처리 - Action callee
 * @param {*} index 변경하고자하는 스크립트 인덱스
 * @param {*} speakerId 변경하고자하는 화자 ID
 * @param {*} voiceName 변경하고자하는 화자 VoiceName
 * @param {*} rowState 변경하고자하는 스크립트 ROW 상태
 */
const updateAtScriptSpeaker = (state, { index, speakerId, voiceName, rowState }) => {
    Object.assign(state.jobScripts[index], { 'speakerId' : speakerId, 'voiceName' : voiceName, 'rowState' : rowState, 'ttsState' : 'W', 'ttsResourceExpiryDate' : null, 'ttsResourceUrl' : null });
};

/**
 * 분리된 스크립트 특정 인덱스 삽입 처리 - Action calle
 * @param {*} index 삽입할 레코드 인덱스 위치
 * @param {*} addScripts 추가하고자하는 스크립트
 * @param {*} deletedScripts 삭제될 스크립트
 */
const insertAtSplitScripts = (state, { index, addJobScripts, deletedJobScripts }) => {
    state.deletedJobScripts.push(...deletedJobScripts);
    state.jobScripts.splice(index, 1, ...addJobScripts);
};

/**
 * 신규 스크립트 생성 처리(alt+enter) - Action callee
 * @param {*} index 신규생성할 인덱스 위치
 * @param {*} script 신규 생성될 스크립트
 */
const insertAtCreateScript = (state, { index, script }) => {
    state.jobScripts.splice(index, 0, script);
};


/**
 * 스크립트 삭제 처리 - Action callee
 * @param {*} 삭제될 인덱스 위치 
 * @param {*} 삭제할 스크립트
 */
const removeAtScript = (state, { index, deletedJobScripts }) => {
    state.deletedJobScripts.push(...deletedJobScripts);

    if ( state.jobScripts.length ) {
        state.jobScripts.splice(index, 1);
    }
};

/**
 * 스크립트 재할당 처리 - Action callee
 * @param {*} targetScripts 재할당할 스크립트
 * @param {*} speakerId 변경될 화자 ID
 * @param {*} speakerNm 변경될 화자 이름
 * @param {*} voiceName 변경될 화자 보이스이름
 */
const setReallocationScripts = (state, {targetScripts, speakerId, speakerNm, voiceName}) => {
    if(targetScripts) {
        targetScripts.forEach((item, index) => {
            const rowIndex = state.jobScripts.findIndex(script => script.scriptId === item.scriptId);
            const targetScript = state.jobScripts[rowIndex];
            const isUpdateCandidate = [ 'S', 'U' ].indexOf(targetScript.rowState) > -1;
            const updateData = {'speakerId' : speakerId, 'speakerNm': speakerNm, 'voiceName': voiceName, 'ttsState' : 'W', 'rowState' : isUpdateCandidate ? 'U' : 'A'};
            Object.assign(state.jobScripts[rowIndex], updateData);
        });
    }
};

export default {
    setJobList,
    insertAtJobScript,
    removeAtJobScript,
    pushDeletedJobScript,
    updateAtJobScriptText,
    updateAtJobScriptFromPayLoad,
    
    updateAtJobScriptTTSReponse,
    updateJobScriptFindByScriptId,


    setJobId,
    setJobTitle,
    clearJobTitle,
    // JOB Script 처리 / START
    
    setJobScripts,

    clearDeletedJobScripts,
    updateAtScriptText,
    updateAtScriptSpeaker,
    insertAtSplitScripts,
    insertAtCreateScript,
    removeAtScript,
    setReallocationScripts,

    // JOB Script 처리 / END
};