
const getLastIndexFromJobScripts = (state) => () => {
    return state.jobScripts.length;
};

const isDeleteCandidate = (state) => (index) => {
    const {rowState} = state.jobScripts[index];
    return [ 'S', 'U' ].indexOf(rowState) > -1;
};

const getJobScriptIndexFindByScriptId = (state) => (searchKey) => {
    return state.jobScripts.findIndex(script => script.scriptId === searchKey);
};

// JOB Script getter(index)
const getJobScriptFindByIndex = (state) => (index)  =>{
    return state.jobScripts[index];
};

export default {
    getLastIndexFromJobScripts,
    isDeleteCandidate,
    getJobScriptIndexFindByScriptId,
    getJobScriptFindByIndex,
};