import Vue from 'vue';
import Vuex from 'vuex';

import base from './modules/base';
import user from './modules/user';
import player from './modules/player';
import job from './modules/job';

import character from './modules/character';

Vue.use(Vuex);

export default new Vuex.Store({

    linkActiveClass: 'active',
    mode: 'hash',
    modules: {
        base,
        character,
        user,
        job,
        player,
    },
    strict: process.env.NODE_ENV !== 'production'
});