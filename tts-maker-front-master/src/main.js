import Vue from 'vue';
import App from './App';
import router from './routes';
import store from './store';
import filter from "@/utils/filters"

import './assets/styles/base.scss'

Vue.config.productionTip = false;
new Vue({
    filter,
    router,
    store,
    el: '#app',
    render: h => h(App),
});