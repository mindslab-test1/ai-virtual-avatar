import Vue from 'vue';
import Router from 'vue-router';

import store from '../store/index';
import Repository from "@/repositories/Repository";
import axios from "axios";

Vue.use(Router);

const alreadyLoggedRouting = (to, from, next) => {
    const userString = localStorage.getItem('user');
    if(userString) {
        return next({'name' : 'MainHomeView'});
    }

    next();
};

const checkForUserAuth = (to, from, next) => {
    const userString = localStorage.getItem('user');
    let loggedIn = false;

    if(userString) {
        const userData = JSON.parse(userString)

        // TODO: access token 체크

        //localStorage.setItem("token", res.data.token)
        //Repository.defaults.headers.common['Authorization'] = `Bearer ${res.data.token}`;
        // store.dispatch("reauth", {
        //     status: "success",
        // })

        store.commit('user/setUserInfo', userData)
        
        loggedIn = true;
    }

    if(!loggedIn) {
        alert('로그인이 필요한 기능입니다.')
        return next({'name' : 'UserSignInView'});
    }

    next()
};


const routes = [
    {
        path: '/',
        redirect: '/users/sign_in'
    },
    {
        path: '/Home',
        name: 'MainHomeView',
        beforeEnter: checkForUserAuth,
        component: require('@/components/main/MainHomeView').default,
        meta: {
            title: 'AVA 에디터',
        }
    },
    {
        path: '/users/sign_up',
        name: 'UserSignUpView',
        component: require('@/components/users/UserSignUpView').default,
        meta: {
            title: 'AVA 회원가입',
        }
    },
    {
        path: '/users/sign_in',
        name: 'UserSignInView',
        beforeEnter: alreadyLoggedRouting,
        component: require('@/components/users/UserSignInView').default,
        meta: {
            title: 'AVA 로그인',
        }
    },
];
const router = new Router({
    //mode: 'history',
    routes: routes
});

const DEFAULT_TITLE = 'AVA Home';
router.afterEach((to, from) => {
    document.title = to.meta.title || DEFAULT_TITLE;
});

export default router;