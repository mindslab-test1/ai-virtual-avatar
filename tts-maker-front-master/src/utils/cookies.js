function saveAuthToCookie(value) {
    document.cookie = `tts_auth=${value}`;
}

function saveUserToCookie(value) {
    document.cookie = `tts_user=${value}`;
}

function getAuthFromCookie() {
    return document.cookie.replace(/(?:(?:^|.*;\s*)tts_auth\s*=\s*([^;]*).*$)|^.*$/, '$1');
}
  
function getUserFromCookie() {
    return document.cookie.replace(/(?:(?:^|.*;\s*)tts_user\s*=\s*([^;]*).*$)|^.*$/, '$1');
}
  
function deleteCookie(value) {
    document.cookie = `${value}=; expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
}

export default { 
    saveAuthToCookie, 
    saveUserToCookie, 
    getAuthFromCookie, 
    getUserFromCookie, 
    deleteCookie 
};